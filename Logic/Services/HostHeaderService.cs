﻿using System;
using Microsoft.Web.Administration;
using System.Linq;

namespace Logic.Services
{
    public class HostHeaderService : IHostHeaderService
    {
        public void AddHostHeader(string websiteName, string fullSubdomainWithPostfix)
        {
            using (var serverManager = new ServerManager())
            {
                Configuration config = serverManager.GetApplicationHostConfiguration();
                ConfigurationSection sitesSection = config.GetSection("system.applicationHost/sites");
                ConfigurationElementCollection sitesCollection = sitesSection.GetCollection();
                ConfigurationElement siteElement = FindElement(sitesCollection, "site", "name", websiteName);

                if (siteElement == null) throw new InvalidOperationException("Website Name not found in IIS!");

                ConfigurationElementCollection bindingsCollection = siteElement.GetCollection("bindings");
                
                if(bindingsCollection.Any(element => (string)element["bindingInformation"] == @"*:80:" + fullSubdomainWithPostfix))
                    return;

                ConfigurationElement bindingElement = bindingsCollection.CreateElement("binding");
                bindingElement["protocol"] = @"http";
                bindingElement["bindingInformation"] = @"*:80:" + fullSubdomainWithPostfix;
                bindingsCollection.Add(bindingElement);

                serverManager.CommitChanges();
            }
        }
        
        public void RemoveHostHeader(string websiteName, string fullSubdomainWithPostfix)
        {
            using (var serverManager = new ServerManager())
            {
                Configuration config = serverManager.GetApplicationHostConfiguration();
                ConfigurationSection sitesSection = config.GetSection("system.applicationHost/sites");
                ConfigurationElementCollection sitesCollection = sitesSection.GetCollection();
                ConfigurationElement siteElement = FindElement(sitesCollection, "site", "name", websiteName);

                if (siteElement == null) throw new InvalidOperationException("Website Name not found in IIS!");

                ConfigurationElementCollection bindingsCollection = siteElement.GetCollection("bindings");
                ConfigurationElement removableElement =
                    bindingsCollection.FirstOrDefault(
                        element => (string) element["bindingInformation"] == @"*:80:" + fullSubdomainWithPostfix);

                if (removableElement != null)
                {
                    bindingsCollection.Remove(removableElement);
                }

                serverManager.CommitChanges();
            }
        }

        private static ConfigurationElement FindElement(ConfigurationElementCollection collection, string elementTagName, params string[] keyValues)
        {
            foreach (ConfigurationElement element in collection)
            {
                if (String.Equals(element.ElementTagName, elementTagName, StringComparison.OrdinalIgnoreCase))
                {
                    bool matches = true;
                    for (int i = 0; i < keyValues.Length; i += 2)
                    {
                        object o = element.GetAttributeValue(keyValues[i]);
                        string value = null;
                        if (o != null)
                        {
                            value = o.ToString();
                        }
                        if (!String.Equals(value, keyValues[i + 1], StringComparison.OrdinalIgnoreCase))
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

    }

    public interface IHostHeaderService 
    {
        void AddHostHeader(string websiteName, string fullSubdomainWithPostfix);
        void RemoveHostHeader(string websiteName, string fullSubdomainWithPostfix);
    }
}