﻿using DAL.FNhibernate.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DAL.FNhibernate.Maps
{
    public class UrlMappingOverride : IAutoMappingOverride<UrlMapping>
    {
        public void Override(AutoMapping<UrlMapping> mapping)
        {
            mapping.Id(urlMapping => urlMapping.Url);
//            mapping.HasOne(urlMapping => urlMapping.Posts)
//                   .Cascade.All();
        }
    }
}