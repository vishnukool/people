﻿using System;

namespace DAL.FNhibernate.Entities
{
    public class Comments
    {
        public virtual int Id { get; set; }
        public virtual Posts Posts { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime Samay { get; set; }
        public virtual string Email { get; set; }
        public virtual string Body { get; set; }
    }
}