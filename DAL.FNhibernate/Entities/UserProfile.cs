﻿using System.Collections.Generic;

namespace DAL.FNhibernate.Entities
{
    public class UserProfile
    {
        public virtual int Id { get; set; }
        public virtual string UserName { get; set; }
        public virtual string FriendlyName { get; set; }
        public virtual IList<Posts> Posts { get; set; }
    }
}