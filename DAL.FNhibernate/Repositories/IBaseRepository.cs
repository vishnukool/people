﻿using System.Collections.Generic;

namespace DAL.FNhibernate.Repositories
{
    public interface IBaseRepository<TEntity, in TId> where TEntity : class
    {
        IList<TEntity> GetAll();

        TEntity GetById(TId id);

        IList<TEntity> GetWithRestriction(Dictionary<string, object> restrictions);

        IList<TEntity> GetForIds(int[] ids);

        void Save(TEntity entity);

        void SaveAll(IList<TEntity> collection);
        void Delete(TEntity entity);
    }
}
