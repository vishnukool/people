﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NHibernate;

namespace DAL.FNhibernate.Repositories
{
    public class BaseRepositoryBla<TEntity, TId> : IBaseRepository<TEntity, TId>
            where TEntity : class 

    {
        private readonly ISessionFactory sessionFactory;

        public BaseRepositoryBla(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }


        public IList<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public TEntity GetById(TId id)
        {
            throw new NotImplementedException();
        }

        public IList<TEntity> GetWithRestriction(Dictionary<string, object> restrictions)
        {
            throw new NotImplementedException();
        }

        public IList<TEntity> GetForIds(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Save(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void SaveAll(IList<TEntity> collection)
        {
            throw new NotImplementedException();
        }

        public void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        protected ISession GetSession()
        {
            ISession session = null;
            try
            {
                
                session = sessionFactory.GetCurrentSession();
            }
            catch (Exception webReqSessionException)
            {
                try
                {
                    session = sessionFactory.GetCurrentSession();
                }
                catch (Exception threadSessionException)
                {
                    Debug.WriteLine("Cannot obtain session from web or thread static context " + webReqSessionException.StackTrace + "\r\nThread Session Exception: " + threadSessionException.StackTrace);
                }
            }

            return session;
        }
    }
}
