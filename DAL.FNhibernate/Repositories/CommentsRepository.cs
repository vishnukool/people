﻿using DAL.FNhibernate.Entities;
using NHibernate;
using NHibernate.Criterion;

namespace DAL.FNhibernate.Repositories
{
    public class CommentsRepository : BaseRepository, ICommentsRepository
    {
        public CommentsRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public Comments GetCommentsWithPost(int id)
        {
            ICriteria criteria = GetSession().CreateCriteria<Comments>().Add(Restrictions.Eq("Id", id));
            criteria.SetFetchMode("Posts", FetchMode.Eager);
            criteria.SetFetchMode("Posts.UserProfile", FetchMode.Eager);
            return criteria.UniqueResult<Comments>();
        }

        public void Delete(Comments comments)
        {
            GetSession().Delete(comments);
        }
       
        
    }

    public interface ICommentsRepository    
    {
        void Delete(Comments comments);
        Comments GetCommentsWithPost(int id);
    }
}