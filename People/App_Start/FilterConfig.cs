﻿using System.Web;
using System.Web.Mvc;
using People.Filters;

namespace People
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new InjectSessionAttribute());
        }
    }
}