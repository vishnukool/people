﻿using System.ComponentModel.DataAnnotations;

namespace People.Models
{
    public class LogOn
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}