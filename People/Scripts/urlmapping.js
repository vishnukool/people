﻿var people = people || {};
people.urlmapping = {
    init: function () {
        this.bindActions();
        this.bindFormValidations();
    },

    calcIframeHeight: function() {
        var theHeight = document.getElementById('iframeBody').contentWindow.document.body.scrollHeight;
        document.getElementById('iframeBody').height = theHeight;
    },

    bindFormValidations: function () {
        $("#commentForm").validate({
            rules: {
                Name: "required",
                Email: {
                    email: true,
                },
                Body: "required"
            }
        });
    },

    bindActions: function () {
       
        $('#commentForm').submit(function() {
            if ($('#commentForm').valid()) {
                $('#commentSubmitInput').hide();
                $('#fakeCommentSubmitInput').show();
            }
        }),

        $('.deleteComment').click(function () {
            $.ajax({
                url: $(this).attr('ajaxUrl'),
                type: 'POST',
                success: function (data) {
                    $("#toastMe").html('Comment deleted !');
                    setTimeout(function() {
                        $("#toastMe").html('');
                    }, 1500);
                    $("#comment_" + data.Id).remove();
                },
                error: function (data) {
                    //todo: parse data and print proper msg
                    $("#toastMe").cftoaster({ content: "Comment already deleted !" });
                    window.location.reload();
                }
            });
            $(this).button('loading');
        });
       
//        $('#deletePost').click(function () {
//            $.ajax({
//                url: $(this).attr('ajaxUrl'),
//                type: 'POST',
//                success: function (data) {
//                    //todo: parse data and then show msg.
//                    $("#toastMe").cftoaster({ content: "Website deleted !" });
//                    setTimeout(function () {
//                        window.location.href = "http://"+ $('#deletePost').attr('homeUrl');
//                    }, 2000);
//                },
//                error: function (data) {
//                    $("#toastMe").cftoaster({ content: "Oops, Rajinikanth is angry with you. Try again in some time !" });
//                    setTimeout(function () {
//                        window.location.reload();
//                    }, 2000);
//                    
//                }
//            });
//        });
    },
    
    addCommentSuccessfull: function (data) {
        if (data.errors !== undefined) {
            $('#addCommentErrors').html(data.errors);
            $('#commentSubmitInput').show();
            $('#fakeCommentSubmitInput').hide();
        }
        else
        window.location.reload();
    },
    addCommentFailure: function () {
        //todo: toast
        alert('oops womething went wrong !');
        window.location.reload();
    }
};

$(document).ready(function () {
        people.urlmapping.init();
});