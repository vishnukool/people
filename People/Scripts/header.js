﻿var people = people || {};
people.header = {
    init: function () {
        this.bindActions();
        this.bindFormValidations();
    },
    
    bindFormValidations: function () {
        $("#editSiteForm").validate({
            rules: {
                title: "required",
                body: "required"
            }
        });

        $("#createSiteForm").validate({
            rules: {
                title: "required",
                subdomain: "required",
                body: "required"
            }
        });
        //$("#subdomain").rules("add", { regex: "^[0-9a-zA-Z]*$" });
    },

    bindActions: function () {

        $("#subdomain").focusout(function () {
            $.ajax({
                url: $(this).attr('data-ajaxurl') + '/' + $(this).val(),
                type: 'POST',
                success: function (data) {
                    if (data.isFree === true) {
                        $("#subdomainAvailability").text('\(is Available\)');                        
                        $("#subdomainAvailability").removeClass();
                        $("#subdomainAvailability").addClass('text-success');
                    }
                    else if ((data.isFree === false)) {
                        $("#subdomainAvailability").text('\(not Available\)');
                        $("#subdomainAvailability").removeClass();
                        $("#subdomainAvailability").addClass('text-error');
                    } else {
                        
                    }
                },
                error: function () {
                }
            });


        });

        $('#editSiteForm').submit(function () {
            if ($('#editSiteForm').valid()) {
                $('#postSubmitInput').hide();
                $('#fakePostSubmitInput').show();
            }
        }),

        $('#createSiteForm').submit(function () {
            if ($('#createSiteForm').valid()) {
                $('#postSubmitInput').hide();
                $('#fakePostSubmitInput').show();
            }
        }),

        $('.deletePost').click(function () {
            $.ajax({
                url: $(this).attr('ajaxUrl'),
                type: 'POST',
                success: function(data) {
                    //todo: parse data and then show msg.
                    if (data.success === true) {
                        $("#toastMe").html('Website deleted !');
                        setTimeout(function() {
                            $("#toastMe").html('');
                            window.location.reload();
                        }, 1200);
                    }
                    else {
                        $("#toastMe").html('Oops, Rajinikanth is angry with you. Try again in some time !');
                        setTimeout(function () {
                            $("#toastMe").html('');
                            window.location.reload();
                        }, 1500);
                    }
                },
                error: function() {
                    $("#toastMe").html('Oops, Rajinikanth is angry with you. Try again in some time !');
                    setTimeout(function () {
                        $("#toastMe").html('');
                        window.location.reload();
                    }, 1500);
                }
            });
        });

        $('#urlSearch').click(function() {
            $("#urlSearchForm").submit();
        });

    },
    
    addCommentFailure: function() {
        alert('comment failed to add');
    }

};

$(document).ready(function () {
        people.header.init();
});