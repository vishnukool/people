﻿using System;
using System.Web.Mvc;

namespace People.Controllers
{
    public class ThrowUpController : Controller
    {
        public ActionResult Index()
        {
            throw new AggregateException();
        }
    }
}
