﻿using System.Configuration;
using System.Web.Mvc;
using DAL.FNhibernate.Entities;
using DAL.FNhibernate.Repositories;

namespace People.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IUserRepository userRepository;
        readonly string domainName = ConfigurationManager.AppSettings["DomanName"];

        public ProfileController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [Authorize]
        public ActionResult Index()
        {
            string userName = User.Identity.Name;
            UserProfile userWithPosts = userRepository.GetUserWithPosts(userName);
            //todo: refactor this to a spearate service that saves domain name in HTTPCntext Item
            ViewBag.DomainNameWithDot = "." + domainName;
            return View(userWithPosts);
        }

    }
}
