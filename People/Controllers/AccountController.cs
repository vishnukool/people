﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Logic.Services;
using Microsoft.Web.WebPages.OAuth;

namespace People.Controllers
{
    public class AccountController : Controller
    {
        private IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        public ActionResult Index(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }
        
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            ClearBloodyCookies();
            return RedirectToAction("Index", "Home");
        }

        private void ClearBloodyCookies()
        {
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);
        }

        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            string url = Url.Action("ExternalLoginCallback", new {returnUrl});
            OAuthWebSecurity.RequestAuthentication(provider, url);

            return new EmptyResult();
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            string url = Url.Action("ExternalLoginCallback", new {returnUrl});
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(url);
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: true))
            {
                return RedirectToLocal(returnUrl);
            }

                // TODO:Increase the time out, errors on slow Internet conn. 
                OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                var fullUserNameWithProvider = GetFullUserNameWithProvider(result);
                
                //Create a user in UserProfile Table
                accountService.CreateUserProfile(fullUserNameWithProvider, result.UserName);
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, fullUserNameWithProvider);
                OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: true);
                return RedirectToLocal(returnUrl);
        }

        private static string GetFullUserNameWithProvider(AuthenticationResult result)
        {
            var fullUserNameWithProvider = result.Provider + "_" + result.ProviderUserId;
            return fullUserNameWithProvider;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return RedirectPermanent(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

/* Dead code, we're not gonna allow R
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(People.Models.Register model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    WebSecurity.Login(model.UserName, model.Password);
                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", e.ToString());
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
*/
    }
    /* Dead code, not using this action anymore
    internal class ExternalLoginResult : ActionResult
    {
        public ExternalLoginResult(string provider, string returnUrl)
        {
            Provider = provider;
            ReturnUrl = returnUrl;
        }

        public string Provider { get; private set; }
        public string ReturnUrl { get; private set; }

        public override void ExecuteResult(ControllerContext context)
        {
            OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
        }
    }*/
}