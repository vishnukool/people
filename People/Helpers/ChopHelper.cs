﻿namespace People.Helpers
{
    public static class ChopHelper
    {
        public static string Chop(this string text, int chopLength, string postfix = "...")
        {
            if (text == null || text.Length < chopLength)
                return text;
            return text.Substring(0, chopLength - postfix.Length) + postfix;
        }
    }
}